# Chess

Chess playing games are well known within programming. This is an example of that.

- [Youtube](https://www.youtube.com/watch?v=PCIGTYP-4f4)
- [GitHub](https://github.com/farhantanvirtushar/Chess)

author:  Farhan Tanvir

refactored by Hans Zuidervaart

## Setup
Before you run the program, make sure you modify the following in Intellij:
- Go to File -->  Settings --> Appearance and Behaviour --> Path Variables
- Add a new line variable by clicking active the + sign and name it PATH_TO_FX and browse to the lib folder of the JavaFX SDK to set its value, and click apply.
- Go to Run --> Edit Configurations
- Type the following in the VM Options section: --module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml
- Click Apply and Run the application. It should work now.

source: [JavaFX and IntelliJ IDEA](https://openjfx.io/openjfx-docs/#IDE-Intellij)

## About
This is a chess engine to demonstrate how that works

Each player begins with a total of sixteen pieces. The pieces that belong to each player are distinguished by color. The lighter colored pieces are referred to as "white," and the player that owns them, "White". The darker colored pieces are referred to as "black", and the player that owns them, "Black". The word "piece" has three meanings, depending on the context. Context should make the intended meaning clear (Burgess 2009:523) (Hooper & Whyld 1992:307).

    It may mean any of the physical pieces of the set, including the pawns. When used this way, "piece" is synonymous with "chessman" (Hooper & Whyld 1992:307) or simply "man" (Hooper & Whyld 1987:200).
    In play, the term is usually used to exclude pawns, referring only to a queen, rook, bishop, knight, or king. In this context, the pieces can be broken down into three groups: major pieces (queen and rook), minor pieces (bishop and knight), and the king (Brace 1977:220).
    In phrases such as "winning a piece", "losing a piece" or "sacrificing a piece", it refers only to a bishop or knight. The queen, rook, and pawn are specified by name in these cases, for example, "winning a queen", "losing a rook", or "sacrificing a pawn" (Just & Burg 2003:5).

In the first context, each of the two players begins with the following sixteen pieces in a standard game:

    1 king
    1 queen
    2 rooks
    2 bishops
    2 knights
    8 pawns

Moves of the pieces
	
It has been suggested that section Moves of the pieces be split out and merged into the article titled Rules of chess, which already exists. (Discuss) (July 2019)
Main article: Rules of chess



Chess starting position. Squares are referenced using algebraic notation.

The rules of chess prescribe the types of move a player can make with each type of chess piece. Each piece type moves in a different way. During play, the players take turns moving one of their own chess pieces.

    The rook moves any number of vacant squares forwards, backwards, left, or right in a straight line. It also takes part, along with the king, in a special move called castling.
    The bishop moves any number of vacant squares diagonally in a straight line. Consequently, a bishop stays on squares of the same color throughout a game. The two bishops each player starts with move on squares of opposite colors.
    The queen moves any number of vacant squares in any direction: forwards, backwards, left, right, or diagonally, in a straight line.
    The king moves exactly one vacant square in any direction: forwards, backwards, left, right, or diagonally; however, it cannot move to a square that is under attack by an opponent, nor can a player make a move with another piece if it will leave the king in check. It also has a special move called castling, in which the king moves two squares towards one of its own rooks and in the same move, the rook jumps over the king to land on the square on the king's other side. Castling may only be performed if the king and rook involved have never previously been moved in the game, if the king is not in check, if the king would not travel through or into check, and if there are no pieces between the rook and the king.
    The knight moves on an extended diagonal from one corner of any 2×3 rectangle of squares to the furthest opposite corner. Consequently, the knight alternates its square color each time it moves. Other than the castling move described above where the rook jumps over the king, the knight is the only piece permitted to routinely jump over any intervening piece(s) when moving.
    The pawn moves forward exactly one square, or optionally, two squares when on its starting square, toward the opponent's side of the board. When there is an enemy piece one square diagonally ahead of a pawn, either left or right, then the pawn may capture that piece. A pawn can perform a special type of capture of an enemy pawn called en passant. If the pawn reaches a square on the back rank of the opponent, it promotes to the player's choice of a queen, rook, bishop, or knight

##A step-by-step guide to building a simple chess AI

by Lauri Hartikka

Let’s explore some basic concepts that will help us create a simple chess AI:

    move-generation
    board evaluation
    minimax
    and alpha beta pruning.

At each step, we’ll improve our algorithm with one of these time-tested chess-programming techniques. I’ll demonstrate how each affects the algorithm’s playing style.

You can view the final AI algorithm here on GitHub.
#####Step 1: Move generation and board visualization

 ![Move options](src/main/resources/images/moveOptions.png)
 
We’ll start by creating a function that just returns a random move from all of the possible moves:

Although this algorithm isn’t a very solid chess player, it’s a good starting point, as we can actually play against it:
Black plays random moves. Playable [here](https://jsfiddle.net/lhartikk/m14epfwb/4)

#####Step 2 : Position evaluation

 ![Points](src/main/resources/images/points.png)
 
Now let’s try to understand which side is stronger in a certain position. The simplest way to achieve this is to count the relative strength of the pieces on the board using the following table:

With the evaluation function, we’re able to create an algorithm that chooses the move that gives the highest evaluation:

The only tangible improvement is that our algorithm will now capture a piece if it can.
Black plays with the aid of the simple evaluation function. Playable [here](https://jsfiddle.net/lhartikk/m5q6fgtb/1/)

#####Step 3: Search tree using Minimax

![MiniMax](src/main/resources/images/searchTree.png)
 
Next we’re going to create a search tree from which the algorithm can chose the best move. This is done by using the Minimax algorithm.

In this algorithm, the recursive tree of all possible moves is explored to a given depth, and the position is evaluated at the ending “leaves” of the tree.

After that, we return either the smallest or the largest value of the child to the parent node, depending on whether it’s a white or black to move. (That is, we try to either minimize or maximize the outcome at each level.)
A visualization of the minimax algorithm in an artificial position. The best move for white is b2-c3, because we can guarantee that we can get to a position where the evaluation is -50

With minimax in place, our algorithm is starting to understand some basic tactics of chess:
Minimax with depth level 2. Playable [here](https://jsfiddle.net/k96eoq0q/1/)

The effectiveness of the minimax algorithm is heavily based on the search depth we can achieve. This is something we’ll improve in the following step.

#####Step 4: Alpha-beta pruning

![Alpha Beta Pruning](src/main/resources/images/alphaBetaPruning.png)
 
Alpha-beta pruning is an optimization method to the minimax algorithm that allows us to disregard some branches in the search tree. This helps us evaluate the minimax search tree much deeper, while using the same resources.

The alpha-beta pruning is based on the situation where we can stop evaluating a part of the search tree if we find a move that leads to a worse situation than a previously discovered move.

The alpha-beta pruning does not influence the outcome of the minimax algorithm — it only makes it faster.

The alpha-beta algorithm also is more efficient if we happen to visit first those paths that lead to good moves.
The positions we do not need to explore if alpha-beta pruning isused and the tree is visited in the described order.

With alpha-beta, we get a significant boost to the minimax algorithm, as is shown in the following example:
The number of positions that are required to evaluate if we want to perform a search with depth of 4 and the “root” position is the one that is shown.

Follow this link to try the alpha-beta improved version of the chess AI.

#####Step 5: Improved evaluation function

 ![Position point eval](src/main/resources/images/positionDependentPoints.png)
 
The initial evaluation function is quite naive as we only count the material that is found on the board. To improve this, we add to the evaluation a factor that takes in account the position of the pieces. For example, a knight on the center of the board is better (because it has more options and is thus more active) than a knight on the edge of the board.

We’ll use a slightly adjusted version of piece-square tables that are originally described in the chess-programming-wiki.
The visualized piece-square tables visualized. We can decrease or increase the evaluation, depending on the location of the piece.

With the following improvement, we start to get an algorithm that plays some “decent” chess, at least from the viewpoint of a casual player:
Improved evaluation and alpha-beta pruning with search depth of 3. Playable [here](https://jsfiddle.net/q76uzxwe/1/)
Conclusions

The strength of even a simple chess-playing algorithm is that it doesn't make stupid mistakes. This said, it still lacks strategic understanding.

With the methods I introduced here, we’ve been able to program a chess-playing-algorithm that can play basic chess. The “AI-part” (move-generation excluded) of the final algorithm is just 200 lines of code, meaning the basic concepts are quite simple to implement. You can check out the final version is on GitHub.

Some further improvements we could make to the algorithm would be for instance:

    move-ordering
    faster move generation
    and end-game specific evaluation.

If you want to learn more, check out the chess programming wiki. It’s a helpful resource for exploring beyond these basic concepts I introduced here.

####Sources
- [Free code camp](https://www.freecodecamp.org/news/simple-chess-ai-step-by-step-1d55a9266977/)

## Notes
Enjoy!

