package hzt.controller;

import hzt.model.GameProperties;
import hzt.model.Position;

import java.util.ArrayList;
import java.util.List;

import static hzt.controller.AppConstants.*;

public class LegalMovesChecker {

    private static final int FOUR = 4;
    private static final int UPPER_ARRAY_BOUND = 8;
    private static final int[][] POSSIBLE_DELTA_MOVES = {{1, 1}, {1, -1}, {-1, 1}, {-1, -1}, {1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    private static final int[][] POSSIBLE_KNIGHT_MOVES = {{1, 2}, {-1, 2}, {1, -2}, {-1, -2}, {2, 1}, {-2, 1}, {2, -1}, {-2, -1}};

    private final GameProperties gameProperties;

    private String[][] board;
    private List<Position> legalMovesList;
    private boolean kNotMoved;
    private boolean rook1NotMoved;
    private boolean rook2NotMoved;

    public LegalMovesChecker(GameProperties gameProperties) {
        this.gameProperties = gameProperties;
    }

    public List<Position> getLegalMoves(int row, int col, String[][] board, CastlingChecker castlingChecker) {
        legalMovesList = new ArrayList<>();
        this.board = board;
        String piece = board[row][col];
        char pieceType = piece.charAt(0);
        char color = piece.charAt(1);
        if (pieceType == PAWN) checkLegalMovesForPawn(color, row, col);
        else if (pieceType == KNIGHT) checkLegalMovesForKnight(color, row, col);
        else if (pieceType == BISHOP) checkLegalMovesForBishop(color, row, col);
        else if (pieceType == ROOK) checkLegalMovesForRook(color, row, col);
        else if (pieceType == QUEEN) checkLegalMovesForQueen(color, row, col);
        if (pieceType == KING) checkLegalMovesForKing(color, row, col, castlingChecker);
        return legalMovesList;
    }

    private void checkLegalMovesForPawn(char color, int row, int col) {
        char playerColor = gameProperties.getPlayerColor();
        if (color == playerColor && row - 1 >= 1) checkPlayerPawn(color, row, col);
        else if (color != playerColor && row + 1 <= CHESS_BOARD_LENGTH) checkCpuPawn(color, row, col);
    }

    private void checkPlayerPawn(char color, int row, int col) {
        boolean startSpotPawn = row == 7 && (board[5][col].equals(EMPTY_SPOT)) && board[6][col].equals(EMPTY_SPOT);
        if (board[row - 1][col].equals(EMPTY_SPOT)) legalMovesList.add(new Position(row - 1, col));
        if (startSpotPawn) legalMovesList.add(new Position(5, col));
        if (col + 1 <= CHESS_BOARD_LENGTH &&
                !board[row - 1][col + 1].equals(EMPTY_SPOT) && board[row - 1][col + 1].charAt(1) != color) {
            legalMovesList.add(new Position(row - 1, col + 1));
        }
        if (col - 1 >= 1 &&
                !board[row - 1][col - 1].equals(EMPTY_SPOT) && board[row - 1][col - 1].charAt(1) != color) {
            legalMovesList.add(new Position(row - 1, col - 1));
        }
    }

    private void checkCpuPawn(char color, int row, int col) {
        boolean startSpotPawn = row == 2 && board[4][col].equals(EMPTY_SPOT) && board[3][col].equals(EMPTY_SPOT);
        if (board[row + 1][col].equals(EMPTY_SPOT)) legalMovesList.add(new Position(row + 1, col));
        if (startSpotPawn) legalMovesList.add(new Position(4, col));
        if (col + 1 <= CHESS_BOARD_LENGTH &&
                !board[row + 1][col + 1].equals(EMPTY_SPOT) && board[row + 1][col + 1].charAt(1) != color) {
            legalMovesList.add(new Position(row + 1, col + 1));
        }
        if (col - 1 >= 1 &&
                !board[row + 1][col - 1].equals(EMPTY_SPOT) && board[row - 1][col - 1].charAt(1) != color) {
            legalMovesList.add(new Position(row + 1, col - 1));
        }
    }

    private void checkLegalMovesForKnight(char color, int curRow, int curCol) {
        for (int[] possibleKnightMove : POSSIBLE_KNIGHT_MOVES) {
            int row = curRow + possibleKnightMove[0];
            int col = curCol + possibleKnightMove[1];
            checkIfSpotCanBeAddedToList(row, col, color);
        }
    }

    private void checkIfSpotCanBeAddedToList(int row, int col, char color) {
        if ((row >= 1) && (row <= CHESS_BOARD_LENGTH) && (col >= 1) && (col <= CHESS_BOARD_LENGTH)) {
            if (board[row][col].equals(EMPTY_SPOT) || board[row][col].charAt(1) != color) {
                legalMovesList.add(new Position(row, col));
            }
        }
    }

    private void checkLegalMovesForBishop(char color, int row, int col) {
        for (int i = 0; i < FOUR; i++) checkPath(color, row, col, POSSIBLE_DELTA_MOVES[i]);
    }

    private void checkLegalMovesForRook(char color, int row, int col) {
        for (int i = FOUR; i < POSSIBLE_DELTA_MOVES.length; i++) {
            checkPath(color, row, col, POSSIBLE_DELTA_MOVES[i]);
        }
    }

    private void checkLegalMovesForQueen(char color, int row, int col) {
        for (int[] possibleMove : POSSIBLE_DELTA_MOVES) checkPath(color, row, col, possibleMove);
    }

    private void checkPath(char color, int curRow, int curCol, int[] deltaMove) {
        int row = curRow + deltaMove[0];
        int col = curCol + deltaMove[1];
        while (row >= 1 && row <= CHESS_BOARD_LENGTH && col >= 1 && col <= CHESS_BOARD_LENGTH) {
            if (board[row][col].equals(EMPTY_SPOT)) legalMovesList.add(new Position(row, col));
            else if (board[row][col].charAt(1) != color) {
                legalMovesList.add(new Position(row, col));
                break;
            } else if (board[row][col].charAt(1) == color) break;
            row = row + deltaMove[0];
            col = col + deltaMove[1];
        }
    }

    private void checkLegalMovesForKing(char color, int curRow, int curCol, CastlingChecker castlingChecker) {
        for (int i = 0; i < UPPER_ARRAY_BOUND; i++) {
            int row = curRow + POSSIBLE_DELTA_MOVES[i][0];
            int col = curCol + POSSIBLE_DELTA_MOVES[i][1];
            checkIfSpotCanBeAddedToList(row, col, color);
        }
        if (isKingFree(color, board)) castlingChecker(color, curRow, curCol, castlingChecker);
    }

    private void castlingChecker(char color, int row, int col, CastlingChecker castlingChecker) {
        kNotMoved = rook1NotMoved = rook2NotMoved = false;
        updateCastlingChecker(color, castlingChecker);
        if (kNotMoved) {
            if (rook1NotMoved) addCastlingMovesToList(color, row, col, -1);
            if (rook2NotMoved) addCastlingMovesToList(color, row, col, 1);
        }
    }

    private void updateCastlingChecker(char color, CastlingChecker castlingChecker) {
        if ((color == WHITE)) {
            kNotMoved = castlingChecker.kwNotMoved;
            rook1NotMoved = castlingChecker.rw1NotMoved;
            rook2NotMoved = castlingChecker.rw2NotMoved;
        } else if ((color == BLACK)) {
            kNotMoved = castlingChecker.kbNotMoved;
            rook1NotMoved = castlingChecker.rb1NotMoved;
            rook2NotMoved = castlingChecker.rb2NotMoved;
        }
    }

    private void addCastlingMovesToList(char color, int row, int col, int dir) {
        boolean b1 = isFree(color, row, col + dir, board);
        boolean b2 = isFree(color, row, col + (2 * dir), board);
        boolean b3 = true;
        if (rook1NotMoved) {
            for (int i = 2; i < col; i++)
                if (!board[row][i].equals(EMPTY_SPOT)) {
                    b3 = false;
                    break;
                }
        } else if (rook2NotMoved) {
            for (int i = 7; i > col; i--)
                if (!(board[row][i].equals(EMPTY_SPOT))) {
                    b3 = false;
                    break;
                }
        }
        if (b1 && b2 && b3) legalMovesList.add(new Position(row, col + (2 * dir)));
    }

    private boolean isFree(char color, int curRow, int curCol, String[][] board) {
        char opponentColor = (color == WHITE) ? BLACK : WHITE;
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                if ((board[row][col].charAt(1) == opponentColor)) {
                    for (Position move : legalMovesList) {
                        if (move.equals(curRow, curCol)) return false;
                    }
                }
            }
        }
        return true;
    }

    private boolean isKingFree(char color, String[][] board) {
        boolean kingFree = true;
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                if ((board[row][col].charAt(0) == KING) && (board[row][col].charAt(1) == color)) {
                    kingFree = isFree(color, row, col, board);
                }
            }
        }
        return kingFree;
    }
}
