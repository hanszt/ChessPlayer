package hzt.controller;

public class CastlingChecker {

    boolean kwNotMoved;
    boolean kbNotMoved;
    boolean rw1NotMoved;
    boolean rw2NotMoved;
    boolean rb1NotMoved;
    boolean rb2NotMoved;

    public CastlingChecker() {
        this.kwNotMoved = true;
        this.kbNotMoved = true;
        this.rw1NotMoved = true;
        this.rw2NotMoved = true;
        this.rb1NotMoved = true;
        this.rb2NotMoved = true;
    }

    public CastlingChecker copy() {
        CastlingChecker copy = new CastlingChecker();
        copy.kbNotMoved = this.kbNotMoved;
        copy.kwNotMoved = this.kwNotMoved;
        copy.rb1NotMoved = this.rb1NotMoved;
        copy.rb2NotMoved = this.rb2NotMoved;
        copy.rw1NotMoved = this.rw1NotMoved;
        copy.rw2NotMoved = this.rw2NotMoved;
        return copy;
    }

    public void setRw1NotMoved(boolean rw1NotMoved) {
        this.rw1NotMoved = rw1NotMoved;
    }

    public void setRb1NotMoved(boolean rb1NotMoved) {
        this.rb1NotMoved = rb1NotMoved;
    }

    public void setRb2NotMoved(boolean rb2NotMoved) {
        this.rb2NotMoved = rb2NotMoved;
    }

    public void setRw2NotMoved(boolean rw2NotMoved) {
        this.rw2NotMoved = rw2NotMoved;
    }

    public void setKwNotMoved(boolean kwNotMoved) {
        this.kwNotMoved = kwNotMoved;
    }

    public void setKbNotMoved(boolean kbNotMoved) {
        this.kbNotMoved = kbNotMoved;
    }
}