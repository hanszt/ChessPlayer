package hzt.controller;

public class AppConstants {

    private AppConstants() {
    }

    public static final int CHESS_BOARD_LENGTH = 8;
    static final int GAME_TREE_HEIGHT = 5;

    public static final char WHITE = 'w';
    public static final char BLACK = 'b';

    public static final char PAWN = 'p';
    static final char KNIGHT = 'n';
    static final char BISHOP = 'b';
    public static final char ROOK = 'r';
    static final char QUEEN = 'q';
    public static final char KING = 'k';

    public static final String WHITE_PAWN = "pw";
    public static final String WHITE_KNIGHT = "nw";
    public static final String WHITE_BISHOP = "bw";
    public static final String WHITE_ROOK = "rw";
    public static final String WHITE_QUEEN = "qw";
    public static final String WHITE_KING = "kw";

    public static final String BLACK_PAWN = "pb";
    public static final String BLACK_KNIGHT = "nb";
    public static final String BLACK_BISHOP = "bb";
    public static final String BLACK_ROOK = "rb";
    public static final String BLACK_QUEEN = "qb";
    public static final String BLACK_KING = "kb";

    public static final String EMPTY_SPOT = "..";

    public static final String BACKGROUND_IMAGE = "gameBackground";

    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    public static final String TITLE = "Chess Solver";
}
