package hzt.controller;

import hzt.model.GameProperties;
import hzt.model.Position;

import java.util.List;

import static hzt.controller.AppConstants.*;
import static hzt.model.GameProperties.swapTwoPiecesByColumn;
import static java.lang.Math.*;
import static java.lang.Math.abs;
import static java.lang.System.*;

public class ComputerMoveGenerator {

    private static final int INIT_POINTS_CPU = Integer.MAX_VALUE;
    private static final int INIT_POINTS_PLAYER = Integer.MIN_VALUE;

    private final GameProperties gameProperties;

    public ComputerMoveGenerator(GameProperties gameProperties) {
        this.gameProperties = gameProperties;
    }

    private int counter;

    public String[][] findBestMove(String[][] board, CastlingChecker cstChecker) {
        double startTime = nanoTime();
        counter = 0;
        BestMoveResult bestResult = new BestMoveResult(INIT_POINTS_CPU, INIT_POINTS_PLAYER, new String[9][9]);
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                evaluateBestMoveCheckingMovesEachCpuPlayerPiece(board, bestResult, cstChecker, row, col);
            }
        }
        out.printf("Number of iterations: %d%n(Points cpu: %d, Points player: %d, computationTime %.2f s)%n%n",
                counter, bestResult.pointsCpu, bestResult.pointsPlayer, (nanoTime() - startTime) / 1e9);
        return bestResult.bestMoveBoard;
    }

    private void evaluateBestMoveCheckingMovesEachCpuPlayerPiece(String[][] curBoard, BestMoveResult result, CastlingChecker cstChecker,
                                                                 int row, int col) {
        if (curBoard[row][col].charAt(1) == gameProperties.getCpuColor()) {
            CastlingChecker cstCpy = cstChecker.copy();
            List<Position> possibleMoves = gameProperties.getMoveChecker().getLegalMoves(row, col, curBoard, cstCpy);
            Position curPosition = new Position(row, col);
            for (Position possibleMove : possibleMoves) {
                String[][] boardCopy = copyBoard(curBoard);
                updateBoardForCastlingAndPawnPromotion(boardCopy, curPosition, possibleMove, cstChecker);
                if (curBoard[possibleMove.getRow()][possibleMove.getColumn()].charAt(0) == KING) return;
                result.pointsCpu = miniMax(boardCopy, cstChecker, INIT_POINTS_PLAYER, INIT_POINTS_CPU, 1);
                if (result.pointsPlayer <= result.pointsCpu) {
                    result.pointsPlayer = result.pointsCpu;
                    result.bestMoveBoard = copyBoard(boardCopy);
                }
            }
        }
    }

    private static class BestMoveResult {

        private int pointsCpu;
        private int pointsPlayer;
        private String[][] bestMoveBoard;

        public BestMoveResult(int pointsCpu, int pointsPlayer, String[][] bestMoveBoard) {
            this.pointsCpu = pointsCpu;
            this.pointsPlayer = pointsPlayer;
            this.bestMoveBoard = bestMoveBoard;
        }
    }

    /**
     * This method tries to minimize the points of the Cpu and maximize the points of the player.
     * The cpu is doing well when the points of the cpu are as low as possible and the points of the player are as high as possible.
     *
     */
    private int miniMax(String[][] board, CastlingChecker castlingChecker, int alpha, int beta, int turnCount) {
        counter++;
        MiniMaxParams p = new MiniMaxParams(INIT_POINTS_CPU, INIT_POINTS_PLAYER, alpha, beta);
        if (turnCount == GAME_TREE_HEIGHT) return getSumPointsBasedOnBoard(board);
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                Integer result = evaluatePossibleMovesCurrentPosition(board, castlingChecker, p, turnCount, row, col);
                if (result != null) return result;
            }
        }
        return pointsByTurnCount(turnCount, p.playerPoints, p.cpuPoints);
    }

    //This method recurses back to miniMax
    private Integer evaluatePossibleMovesCurrentPosition(String[][] board, CastlingChecker castlingChecker, MiniMaxParams p,
                                                         int turnCount, int row, int col) {
        char color = turnCount % 2 == 0 ? gameProperties.getCpuColor() : gameProperties.getPlayerColor();
        if (board[row][col].charAt(1) == color) {
            List<Position> legalMoves = gameProperties.getMoveChecker().getLegalMoves(row, col, board, castlingChecker.copy());
            Position curPosition = new Position(row, col);
            for (Position legalNextMove : legalMoves) {
                if (p.beta <= p.alpha) return pointsByTurnCount(turnCount, p.playerPoints, p.cpuPoints);
                if (kingStroked(legalNextMove, board)) return miniMaxPointsWhenKingStroked(color);
                String[][] boardCopy = copyBoard(board);
                updateBoardForCastlingAndPawnPromotion(boardCopy, curPosition, legalNextMove, castlingChecker);
                int miniMaxResult = miniMax(boardCopy, castlingChecker, p.alpha, p.beta, turnCount + 1);
                p.updateBasedOnMiniMaxResult(miniMaxResult, turnCount);
            }
        }
        return null;
    }

    private static class MiniMaxParams {

        private int cpuPoints;
        private int playerPoints;
        private int alpha;
        private int beta;

        public MiniMaxParams(int cpuPoints, int playerPoints, int alpha, int beta) {
            this.cpuPoints = cpuPoints;
            this.playerPoints = playerPoints;
            this.alpha = alpha;
            this.beta = beta;
        }

        private void updateBasedOnMiniMaxResult(int miniMaxResult, int turnCount) {
            if (turnCount % 2 == 0) {
                playerPoints = max(playerPoints, miniMaxResult);
                alpha = max(alpha, miniMaxResult);
            } else {
                cpuPoints = min(cpuPoints, miniMaxResult);
                beta = min(beta, miniMaxResult);
            }
        }
    }

    private boolean kingStroked(Position legalNextMove, String[][] board) {
        return board[legalNextMove.getRow()][legalNextMove.getColumn()].charAt(0) == KING;
    }

    private int miniMaxPointsWhenKingStroked(char color) {
        return color == gameProperties.getPlayerColor() ? INIT_POINTS_PLAYER : INIT_POINTS_CPU;
    }

    private int pointsByTurnCount(int turnCount, int playerPoints, int cpuPoints) {
        return turnCount % 2 == 0 ? playerPoints : cpuPoints;
    }

    private String[][] copyBoard(String[][] board) {
        String[][] boardCopy = new String[9][9];
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            arraycopy(board[row], 1, boardCopy[row], 1, CHESS_BOARD_LENGTH);
        }
        return boardCopy;
    }

    private int getSumPointsBasedOnBoard(String[][] board) {
        int sumPoints = 0;
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                int points;
                switch (board[row][col]) {
                    case EMPTY_SPOT:
                        points = 0;
                        break;
                    case WHITE_PAWN:
                        points = -10;
                        break;
                    case BLACK_PAWN:
                        points = 10;
                        break;
                    case WHITE_KNIGHT:
                        points = -30;
                        break;
                    case BLACK_KNIGHT:
                        points = 30;
                        break;
                    case WHITE_BISHOP:
                        points = -35;
                        break;
                    case BLACK_BISHOP:
                        points = 35;
                        break;
                    case WHITE_ROOK:
                        points = -50;
                        break;
                    case BLACK_ROOK:
                        points = 50;
                        break;
                    case WHITE_QUEEN:
                        points = -90;
                        break;
                    case BLACK_QUEEN:
                        points = 90;
                        break;
                    case WHITE_KING:
                        points = -900;
                        break;
                    case BLACK_KING:
                        points = 900;
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + board[row][col]);
                }
                sumPoints += points;
            }
        }
        if (gameProperties.getCpuColor() == WHITE) sumPoints = -sumPoints;
        return sumPoints;
    }

    private void updateBoardForCastlingAndPawnPromotion(String[][] board, Position curSpot, Position evalSpot, CastlingChecker cstChecker) {
        String piece = board[curSpot.getRow()][curSpot.getColumn()];
        if (piece.charAt(0) == KING) {
            updateCastlingCheckerForKing(piece, cstChecker);
            makeCastlingMove(curSpot, evalSpot, board);
        } else if (piece.charAt(0) == ROOK) {
            updateCastlingCheckerForRooks(piece, curSpot, cstChecker);
        } else {
            board[evalSpot.getRow()][evalSpot.getColumn()] = piece;
            board[curSpot.getRow()][curSpot.getColumn()] = EMPTY_SPOT;
            checkIfPawnCanBePromotedToQueen(evalSpot, board);
        }
    }

    private void makeCastlingMove(Position curSpot, Position newSpot, String[][] board) {
        if (abs(curSpot.getColumn() - newSpot.getColumn()) == 2) {
            if (newSpot.getColumn() == 7) swapTwoPiecesByColumn(board, curSpot.getRow(), 6, 8);
            else if (newSpot.getColumn() == 3) swapTwoPiecesByColumn(board, curSpot.getRow(), 4, 1);
        }
    }

    private void updateCastlingCheckerForKing(String piece, CastlingChecker cstChecker) {
        if (piece.charAt(1) == WHITE) cstChecker.setKwNotMoved(false);
        else if (piece.charAt(1) == BLACK) cstChecker.setKbNotMoved(false);
    }

    private void updateCastlingCheckerForRooks(String piece, Position newSpot, CastlingChecker castlingChecker) {
        if (piece.charAt(1) == WHITE) {
            if (newSpot.getColumn() == 1) castlingChecker.setRw1NotMoved(false);
            else if (newSpot.getColumn() == 8) castlingChecker.setRw2NotMoved(false);
        } else if (piece.charAt(1) == BLACK) {
            if (newSpot.getColumn() == 1) castlingChecker.setRb1NotMoved(false);
            else if (newSpot.getColumn() == 8) castlingChecker.setRb2NotMoved(false);
        }
    }

    private void checkIfPawnCanBePromotedToQueen(Position newSpot, String[][] board) {
        boolean newSpotAtBorderRow = (newSpot.getRow() == 1) || (newSpot.getRow() == 8);
        boolean pieceIsPawn = board[newSpot.getRow()][newSpot.getColumn()].charAt(0) == PAWN;
        if (newSpotAtBorderRow && pieceIsPawn) {
            if (board[newSpot.getRow()][newSpot.getColumn()].charAt(1) == WHITE) {
                board[newSpot.getRow()][newSpot.getColumn()] = WHITE_QUEEN;
            } else if (board[newSpot.getRow()][newSpot.getColumn()].charAt(1) == BLACK) {
                board[newSpot.getRow()][newSpot.getColumn()] = BLACK_QUEEN;
            }
        }
    }
}
