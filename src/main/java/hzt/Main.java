package hzt;

import hzt.model.GameProperties;
import hzt.view.Menu;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Chess");
        GameProperties gameProperties = new GameProperties(stage);
        stage.setWidth(gameProperties.getMaxWidth());
        stage.setHeight(gameProperties.getMaxHeight());
        stage.setMinWidth(gameProperties.getMaxWidth());
        stage.setMinHeight(gameProperties.getMaxHeight());
        Scene menuScene = new Scene(new Menu(gameProperties));
        stage.setScene(menuScene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
