package hzt.model;

import hzt.controller.CastlingChecker;
import hzt.controller.ComputerMoveGenerator;
import hzt.controller.LegalMovesChecker;
import hzt.view.ChessBoardInitializer;
import hzt.view.GameOverWindow;
import hzt.view.MoveHandler;
import hzt.view.PlayPane;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static hzt.controller.AppConstants.*;
import static java.lang.Math.abs;

public class GameProperties {

    private final double maxWidth;
    private final double maxHeight;
    private final double squareSize;

    private final Stage primaryStage;
    private final Button[][] boardView;
    private final Map<String, Image> imageMap;
    private final LegalMovesChecker moveChecker;
    private final CastlingChecker cstChecker;
    private final ComputerMoveGenerator computerMoveGenerator;

    private boolean gameOver;
    private char cpuColor;
    private char playerColor;

    private ChessBoardInitializer chessBoardInitializer;
    private Position curSpot;
    private Position newSpot;
    private List<Position> nextPossibleMoves;
    private final PlayPane playPane;

    private int numberOfMoves = 0;

    public GameProperties(Stage stage) {
        playPane = new PlayPane(this);
        gameOver = false;
        cstChecker = new CastlingChecker();
        primaryStage = stage;
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        maxHeight = primaryScreenBounds.getHeight();
        maxWidth = primaryScreenBounds.getHeight();
        squareSize = maxHeight / 8.5;
        boardView = new Button[9][9];
        imageMap = fillImageMap();
        moveChecker = new LegalMovesChecker(this);
        computerMoveGenerator = new ComputerMoveGenerator(this);
    }

    private HashMap<String, Image> fillImageMap() {
        HashMap<String, Image> imageMap = new HashMap<>();
        File fileDir = new File(System.getProperty("user.dir") + "/src/main/resources/input");
        String[] fileNames = fileDir.list();
        for (String image : Objects.requireNonNull(fileNames)) {
            String key = image.split("[.]")[0];
            String fileLocation = new File(System.getProperty("user.dir") + "/src/main/resources/input/" + image).toURI().toString();
            if (key.equals(BACKGROUND_IMAGE)) {
                imageMap.put(key, new Image(fileLocation, maxWidth, maxHeight, true, true));
            } else imageMap.put(key, new Image(fileLocation, squareSize, squareSize, true, true));
        }
        return imageMap;
    }

    public void setPlayerWhite() {
        cpuColor = BLACK;
        playerColor = WHITE;
        chessBoardInitializer = new ChessBoardInitializer(this);
//        chessBoard.printBoard();
        primaryStage.setScene(new Scene(playPane));
        setupBeginningOfGame();
    }

    public void setPlayerBlack() {
        cpuColor = WHITE;
        playerColor = BLACK;
        chessBoardInitializer = new ChessBoardInitializer(this);
//        chessBoard.printBoard();
        primaryStage.setScene(new Scene(playPane));
        setupBeginningOfGame();
    }

    private void setupBackgroundView() {
        BackgroundImage backgroundImage = new BackgroundImage(imageMap.get(BACKGROUND_IMAGE),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        playPane.setBackground(new Background(backgroundImage));
    }

    private void setupBeginningOfGame() {
        setupBackgroundView();
        setupBoard(this);
        if (cpuColor == WHITE) pcMakeMove();
        updateBoard();
    }

    private void setupBoard(GameProperties gameProperties) {
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                boardView[row][col] = new Button("");
                boardView[row][col].setMaxSize(squareSize, squareSize);
                boardView[row][col].setMinSize(squareSize, squareSize);
                boardView[row][col].setOnAction(new MoveHandler(row, col, gameProperties));
                playPane.add(boardView[row][col], col, row - 1);
            }
        }
    }

    public void playerMakeMove(Position selectedSpot) {
        if (curSpot == null) {
            curSpot = selectedSpot;
            showLegalMovesForPlayer();
        } else {
            newSpot = selectedSpot;
            executePlayerMove();
        }
    }

    private void showLegalMovesForPlayer() {
        nextPossibleMoves = getMoveChecker().getLegalMoves(curSpot.getRow(), curSpot.getColumn(), chessBoardInitializer.getChessBoard(), cstChecker);
        for (Position position : nextPossibleMoves) {
            if (position != null) {
                int row = position.getRow(), column = position.getColumn();
                if ((row + column) % 2 == 0) boardView[row][column].setStyle("-fx-background-color: #69ff69;");
                else boardView[row][column].setStyle("-fx-background-color: #005500;");
            }
        }
    }

    private void executePlayerMove() {
        boolean playerMadeMove = false;
        String[][] board = chessBoardInitializer.getChessBoard();
        for (Position legalMove : nextPossibleMoves) {
            if (newSpot.equals(legalMove)) {
                gameOverIfKingTaken(board);
                String piece = board[curSpot.getRow()][curSpot.getColumn()];
                if (piece.charAt(0) == KING) makeCastlingMoveKing(board, piece);
                else if (piece.charAt(0) == ROOK) updateCastlingCheckerRooks(piece);
                makeMove(board, piece);
                playerMadeMove = true;
                break;
            }
        }
        updateBoard();
        curSpot = newSpot = null;
        if (playerMadeMove && !gameOver) pcMakeMove();
    }

    private void makeMove(String[][] board, String piece) {
        board[newSpot.getRow()][newSpot.getColumn()] = piece;
        board[curSpot.getRow()][curSpot.getColumn()] = EMPTY_SPOT;
        checkForUpgradeOfPawn(board);
        numberOfMoves++;
    }

    private void checkForUpgradeOfPawn(String[][] board) {
        if (newSpot.getRow() == 1 || newSpot.getRow() == CHESS_BOARD_LENGTH &&
                board[newSpot.getRow()][newSpot.getColumn()].charAt(0) == PAWN) {
            if (board[newSpot.getRow()][newSpot.getColumn()].charAt(1) == WHITE) {
                board[newSpot.getRow()][newSpot.getColumn()] = WHITE_QUEEN;
            } else if (board[newSpot.getRow()][newSpot.getColumn()].charAt(1) == BLACK) {
                board[newSpot.getRow()][newSpot.getColumn()] = BLACK_QUEEN;
            }
        }
    }

    private void updateCastlingCheckerRooks(String piece) {
        if (piece.charAt(1) == WHITE) {
            if (curSpot.getColumn() == 1) cstChecker.setRw1NotMoved(false);
            else if (curSpot.getColumn() == CHESS_BOARD_LENGTH) cstChecker.setRw2NotMoved(false);
        } else if (piece.charAt(1) == BLACK) {
            if (curSpot.getColumn() == 1) cstChecker.setRb1NotMoved(false);
            else if (curSpot.getColumn() == CHESS_BOARD_LENGTH) cstChecker.setRb2NotMoved(false);
        }
    }

    private void makeCastlingMoveKing(String[][] board, String piece) {
        if (piece.charAt(1) == WHITE) cstChecker.setKwNotMoved(false);
        else if (piece.charAt(1) == BLACK) cstChecker.setKbNotMoved(false);
        if (abs(curSpot.getColumn() - newSpot.getColumn()) == 2) {
            if (newSpot.getColumn() == 7) swapTwoPiecesByColumn(board, curSpot.getRow(), 6, 8);
            else if (newSpot.getColumn() == 3) swapTwoPiecesByColumn(board, curSpot.getRow(), 4, 1);
            else if (newSpot.getColumn() == 2) swapTwoPiecesByColumn(board, curSpot.getRow(), 3, 1);
            else if (newSpot.getColumn() == 6) swapTwoPiecesByColumn(board, curSpot.getRow(), 5, 8);
        }
    }

    private void gameOverIfKingTaken(String[][] board) {
        if (board[newSpot.getRow()][newSpot.getColumn()].charAt(0) == KING) {
            gameOver = true;
            showGameOverWindow();
        }
    }

    private void pcMakeMove() {
        System.out.println("Move number: " + ++numberOfMoves);
        System.out.println("Computer is calculating best move...");
        String[][] nextBoard = computerMoveGenerator.findBestMove(chessBoardInitializer.getChessBoard(), cstChecker.copy());
        chessBoardInitializer.printBoard(nextBoard);
        System.out.println(DOTTED_LINE);
        int kingCounter = 0;
        for (int i = 1; i <= CHESS_BOARD_LENGTH; i++) {
            for (int j = 1; j <= CHESS_BOARD_LENGTH; j++) {
                chessBoardInitializer.getChessBoard()[i][j] = nextBoard[i][j];
                if (chessBoardInitializer.getChessBoard()[i][j].charAt(0) == KING) kingCounter++;
            }
        }
        gameOverIfLessThenTwoKingsOnBoard(kingCounter);
        updateBoard();
    }

    private void gameOverIfLessThenTwoKingsOnBoard(int kingCounter) {
        if (kingCounter < 2) {
            gameOver = true;
            showGameOverWindow();
        }
    }

    private void updateBoard() {
        for (int row = 1; row <= CHESS_BOARD_LENGTH; row++) {
            for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
                if ((row + col) % 2 == 0) boardView[row][col].setStyle("-fx-background-color: #ffffff;");
                else boardView[row][col].setStyle("-fx-background-color: #696969;");
                String pieceType = chessBoardInitializer.getChessBoard()[row][col];
                boardView[row][col].setGraphic(new ImageView(imageMap.get(pieceType)));
                if (pieceType.equals(EMPTY_SPOT)) boardView[row][col].setGraphic(new ImageView());
            }
        }
    }

    public static void swapTwoPiecesByColumn(String[][] board, int row, int col1, int col2) {
        String piece = board[row][col1];
        board[row][col1] = board[row][col2];
        board[row][col2] = piece;
    }

    private void showGameOverWindow() {
        Stage finish = new Stage();
        finish.setScene(new Scene(new GameOverWindow()));
        finish.show();
    }

    public Map<String, Image> getImageMap() {
        return imageMap;
    }

    public double getMaxWidth() {
        return maxWidth;
    }

    public double getMaxHeight() {
        return maxHeight;
    }

    public LegalMovesChecker getMoveChecker() {
        return moveChecker;
    }

    public char getCpuColor() {
        return cpuColor;
    }

    public char getPlayerColor() {
        return playerColor;
    }

    public boolean isNotGameOver() {
        return !gameOver;
    }
}
