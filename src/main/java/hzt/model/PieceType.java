package hzt.model;

public enum PieceType {

    EMPTY('#', "Empty", 0),
    PAWN('p', "Pawn", 10),
    KNIGHT('n', "Knight", 30),
    BISHOP('b',"Bishop", 35),
    ROOK('r', "Rook", 50),
    QUEEN('q', "Queen", 90),
    KING('k', "King", 900);

    private final char code;
    private final String name;
    private final int value;

    PieceType(char code, String name, int value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }

    public char getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
