package hzt.model;

public class Piece {

    private final PieceType type;
    private final Color color;

    private Position position;

    public Piece(PieceType type, Color color, Position position) {
        this.type = type;
        this.color = color;
        this.position = position;
    }

    public PieceType getType() {
        return type;
    }

    public Color getColor() {
        return color;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public enum Color {

        WHITE('w', "White", javafx.scene.paint.Color.WHITE),
        BLACK('b', "Black", javafx.scene.paint.Color.BLACK);

        private final char code;
        private final String name;
        private final javafx.scene.paint.Color fxColor;

        Color(char code, String name, javafx.scene.paint.Color fxColor) {
            this.code = code;
            this.name = name;
            this.fxColor = fxColor;
        }

        public char getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public javafx.scene.paint.Color getFxColor() {
            return fxColor;
        }
    }
}
