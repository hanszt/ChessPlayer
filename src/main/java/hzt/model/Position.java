package hzt.model;

public class Position {

    private final int row;
    private final int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public boolean equals(int row, int column) {
        return this.row == row && this.column == column;
    }

    boolean equals(Position position) {
        return equals(position.getRow(), position.getColumn());
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }
}
