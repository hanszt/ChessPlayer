package hzt.view;

import hzt.model.GameProperties;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import static hzt.controller.AppConstants.*;
import static java.lang.System.out;
import static javafx.scene.layout.BackgroundRepeat.NO_REPEAT;
import static javafx.scene.layout.BackgroundRepeat.REPEAT;
import static javafx.scene.paint.Color.BLACK;
import static javafx.scene.text.Font.font;

public class Menu extends GridPane {

    public Menu(GameProperties gameProperties) {
        setVgap(20);
        setHgap(100);
        BackgroundImage backgroundImage = new BackgroundImage(gameProperties.getImageMap().get(BACKGROUND_IMAGE),
                REPEAT, NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        setBackground(new Background(backgroundImage));
        setupLabelAndStartUpButtons(gameProperties);
        out.println("\n\n" + DOTTED_LINE + "\n" + TITLE);
        out.println("This program enables you to play chess against the computer. Let's see if you can beat it ;)");
        out.println(DOTTED_LINE);
    }

    private void setupLabelAndStartUpButtons(GameProperties gameProperties) {
        Label playAs = new Label("Play as");
        Button white = new Button("", new ImageView(gameProperties.getImageMap().get(WHITE_PAWN)));
        Button black = new Button("", new ImageView(gameProperties.getImageMap().get(BLACK_PAWN)));
        playAs.setTextFill(BLACK);
        playAs.setFont(font("Arial", 50));
        add(playAs, 2, 5);
        add(white, 2, 8);
        add(black, 2, 11);
        setPlayerColor(gameProperties, white, black);
    }


    private void setPlayerColor(GameProperties gameProperties, Button white, Button black) {
        white.setOnAction(e -> gameProperties.setPlayerWhite());
        black.setOnAction(e -> gameProperties.setPlayerBlack());
    }
}
