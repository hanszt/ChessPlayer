package hzt.view;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

import static javafx.scene.paint.Color.BLACK;

public class GameOverWindow extends GridPane {

    public GameOverWindow() {
        Label gameOver = new Label("Game Over");
        add(gameOver,3,2);
        gameOver.setTextFill(BLACK);
        gameOver.setFont(Font.font("Arial", 50));
    }
}
