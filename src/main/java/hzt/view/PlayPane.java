package hzt.view;

import hzt.model.GameProperties;
import javafx.scene.layout.GridPane;

public class PlayPane extends GridPane {

    final GameProperties gameProperties;

    public PlayPane(GameProperties gameProperties) {
        this.gameProperties = gameProperties;
    }
}