package hzt.view;


import hzt.model.GameProperties;
import hzt.model.Position;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class MoveHandler implements EventHandler<ActionEvent> {

    private final Position position;
    private final GameProperties gameProperties;

    public MoveHandler(int row, int col, GameProperties gameProperties) {
        position = new Position(row, col);
        this.gameProperties = gameProperties;
    }

    @Override
    public void handle(ActionEvent event) {
        if (gameProperties.isNotGameOver()) gameProperties.playerMakeMove(position);
    }
}
