package hzt.view;

import hzt.model.GameProperties;

import static hzt.controller.AppConstants.*;

public class ChessBoardInitializer {

    private final String[][] chessBoard;

    public ChessBoardInitializer(GameProperties gameProperties) {
        String[][] emptyBoard = getEmptyBoard();
        String[][] boardWithPawns = placePawnsOnBoard(emptyBoard, gameProperties);
        this.chessBoard = placeOtherPiecesOnTheBoard(boardWithPawns, gameProperties.getPlayerColor());
    }

    private String[][] getEmptyBoard() {
        String[][] board = new String[9][9];
        for (int i = 1; i <= CHESS_BOARD_LENGTH; i++) {
            for (int j = 1; j <= CHESS_BOARD_LENGTH; j++) {
                board[i][j] = EMPTY_SPOT;
            }
        }
        return board;
    }

    private String[][] placePawnsOnBoard(String[][] board, GameProperties gameProperties) {
        for (int col = 1; col <= CHESS_BOARD_LENGTH; col++) {
            if (gameProperties.getPlayerColor() == WHITE) {
                board[2][col] = BLACK_PAWN;
                board[7][col] = WHITE_PAWN;
            } else {
                board[2][col] = WHITE_PAWN;
                board[7][col] = BLACK_PAWN;
            }
        }
        return board;
    }

    private String[][] placeOtherPiecesOnTheBoard(String[][] board, char playerColor) {
        placeRooksAndKnights(board, playerColor);
        placeBishopsKingAndQueen(board, playerColor);
        return board;
    }

    private void placeRooksAndKnights(String[][] board, char playerColor) {
        board[1][1] = playerColor == WHITE ? BLACK_ROOK : WHITE_ROOK;
        board[1][8] = playerColor == WHITE ? BLACK_ROOK : WHITE_ROOK;
        board[8][1] = playerColor == WHITE ? WHITE_ROOK : BLACK_ROOK;
        board[8][8] = playerColor == WHITE ? WHITE_ROOK : BLACK_ROOK;

        board[1][2] = playerColor == WHITE ? BLACK_KNIGHT : WHITE_KNIGHT;
        board[1][7] = playerColor == WHITE ? BLACK_KNIGHT : WHITE_KNIGHT;
        board[8][2] = playerColor == WHITE ? WHITE_KNIGHT : BLACK_KNIGHT;
        board[8][7] = playerColor == WHITE ? WHITE_KNIGHT : BLACK_KNIGHT;
    }

    private void placeBishopsKingAndQueen(String[][] board, char playerColor) {
        board[1][3] = playerColor == WHITE ? BLACK_BISHOP : WHITE_BISHOP;
        board[1][6] = playerColor == WHITE ? BLACK_BISHOP : WHITE_BISHOP;
        board[8][3] = playerColor == WHITE ? WHITE_BISHOP : BLACK_BISHOP;
        board[8][6] = playerColor == WHITE ? WHITE_BISHOP : BLACK_BISHOP;

        board[1][4] = playerColor == WHITE ? BLACK_QUEEN : WHITE_QUEEN;
        board[1][5] = playerColor == WHITE ? BLACK_KING : WHITE_KING;
        board[8][4] = playerColor == WHITE ? WHITE_QUEEN : BLACK_QUEEN;
        board[8][5] = playerColor == WHITE ? WHITE_KING : BLACK_KING;
    }

    public void printBoard(String[][] board) {
        for (int row = 1; row < board.length; row++) {
            for (int col = 1; col < board[0].length; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public String[][] getChessBoard() {
        return chessBoard;
    }
}
